/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import com.itextpdf.text.DocumentException;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import javax.swing.filechooser.FileFilter;
import org.gjt.sp.jedit.textarea.StandaloneTextArea;

public class Directorios extends JPanel
        implements ActionListener {

    JFileChooser chooser;
    String choosertitle, content;
    MiFrame frame;

    public Directorios(String accion, MiFrame frame, String typeOfSave) throws IOException, DocumentException {
        WindowUtilities.setNativeLookAndFeel();
        this.frame = frame;
        
        StandaloneTextArea txt = (StandaloneTextArea) frame.vector_textArea.get(frame.tabbedPane.getSelectedIndex());

        chooser = new JFileChooser();
        chooser.setCurrentDirectory(new java.io.File(frame.ws.getWorkspaceWorkingPath()));
        chooser.setDialogTitle(choosertitle);
        chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        chooser.setAcceptAllFileFilterUsed(false);
        
        frame.text=txt;//////////

        if ("Load".equals(accion)) {
            chooser.setFileFilter(new TypeOfFile2());
            chooser.setFileFilter(new TypeOfFile1());
            if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
                if (chooser.getFileFilter() instanceof TypeOfFile1) {
                    frame.text.setName(".vhd");
                    frame.text.extension = ".vhd";
                    //frame.vector_textArea.add(frame.text);

                    System.out.println("VHDL");
                } else if (chooser.getFileFilter() instanceof TypeOfFile2) {
                    frame.text.setName(".v");
                    frame.text.extension = ".v";
                    System.out.println("VERILOG");
                }
                Load_File load_file = new Load_File("" + chooser.getSelectedFile(), frame);

            }
        } else if ("Save".equals(accion)) {
            chooser.setFileFilter(new TypeOfFile2());
            chooser.setFileFilter(new TypeOfFile1());
            if (chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
                //////////frame.text=txt;
                
                if (chooser.getFileFilter() instanceof TypeOfFile1) {
                    frame.text.setName(".vhd");
                } else if (chooser.getFileFilter() instanceof TypeOfFile2) {
                    frame.text.setName(".v");
                    //frame.vector_textArea.add(frame.text);
                }
                if(frame.tabbedPane.getTitleAt(frame.tabbedPane.getSelectedIndex()).equals("blank")){
                    frame.vector_textArea.add(frame.text);
                    frame.vector_textArea.remove(0);
                }
                File fileToSave = chooser.getSelectedFile();
                Save_File save_file = new Save_File(fileToSave.getAbsolutePath() + frame.text.getName(), frame, typeOfSave, false);
                frame.tabbedPane.setTitleAt(frame.tabbedPane.getSelectedIndex(), fileToSave.getName() + frame.text.getName());
            }
        } else if("Save_PDF".equals(accion)){
            chooser.setFileFilter(new TypeOfFilePDF());
            if (chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
                File pdfToSave=chooser.getSelectedFile();
                Save_File save_pdf=new Save_File(pdfToSave.getAbsolutePath()+".pdf", frame, typeOfSave, true);
            }
        }
    }
    
    public Dimension getPreferredSize() {
        return new Dimension(200, 200);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}

class TypeOfFile1 extends FileFilter {

    public boolean accept(File f) {
        return f.isDirectory() || f.getName().toLowerCase().endsWith(".vhd");
    }

    //Set description for the type of file that should be display  
    public String getDescription() {
        return ".vhd files";
    }
}

class TypeOfFile2 extends FileFilter {
    //Type of file that should be display in JFileChooser will be set here  
    //We choose to display only directory and text file  

    public boolean accept(File f) {
        return f.isDirectory() || f.getName().toLowerCase().endsWith(".v");
    }

    //Set description for the type of file that should be display  
    public String getDescription() {
        return ".v files";
    }
}
    class TypeOfFilePDF extends FileFilter {
    //Type of file that should be display in JFileChooser will be set here  
    //We choose to display only directory and text file  

    public boolean accept(File f) {
        return f.isDirectory() || f.getName().toLowerCase().endsWith(".pdf");
    }

    //Set description for the type of file that should be display  
    public String getDescription() {
        return ".pdf files";
    }
    }
