/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/**
 *
 * @author mario
 */
class PopUpMenu extends JPopupMenu {
    JMenuItem open_PU, delete_PU, compile_PU;
    MiFrame frame;
    String projectname;
    public PopUpMenu(MiFrame frame){
        this.frame=frame;
        
        open_PU = new JMenuItem("Open");
        compile_PU=new JMenuItem("Verify/Compile");
        delete_PU=new JMenuItem("Delete");
        add(open_PU);
        add(compile_PU);
        add(delete_PU);
        MenuListener listener=new MenuListener(frame);
        open_PU.addActionListener(listener);
        compile_PU.addActionListener(listener);
        delete_PU.addActionListener(listener);
    }
}