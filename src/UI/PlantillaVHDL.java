/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author mario
 */
public class PlantillaVHDL {
    String plantilla;
    
    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    Date date = new Date();
    String fecha=dateFormat.format(date);    
    PlantillaVHDL(String projectname, String modulename){
        plantilla="----------------------------------------------------------------------------------\n-- Company: Universidad Miguel Hernández\n-- Engineer: "+System.getProperty("user.name")+"\n-- \n-- Create Date:    "+fecha+" \n-- Design Name: \n-- Module Name:    "+modulename+" - Behavioral \n-- Project Name: "+projectname+"\n-- Target Devices: \n-- Tool versions: \n-- Description: \n--\n-- Dependencies: \n--\n-- Revision: \n-- Revision 0.01 - File Created\n-- Additional Comments: \n--\n----------------------------------------------------------------------------------\nlibrary IEEE;\nuse IEEE.STD_LOGIC_1164.ALL;\nuse IEEE.STD_LOGIC_ARITH.ALL;\n\nuse IEEE.STD_LOGIC_UNSIGNED.ALL;\n\n---- Uncomment the following library declaration if instantiating|n---- any Xilinx primitives in this code.\n--library UNISIM;\n--use UNISIM.VComponents.all;\n\nentity "+modulename+" is\nend "+modulename+";\n\narchitecture Behavioral of "+modulename+" is\n\nbegin\n\n\nend Behavioral;";
    }
}
