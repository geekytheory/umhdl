package UI;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreePath;

/**
 *
 * @author mario
 */
public final class FileTree extends JPanel {

    /**
     * Construct a FileTree
     */
    MiFrame frame;
    JTree tree;
    String project, filename;
    PopUpMenu popup;
    int selRow;
    TreePath selPath;

    public FileTree(File dir, final MiFrame frame) {
        // Make a tree list with all the nodes, and make it a JTree
        this.frame = frame;
        tree = new JTree(addNodes(null, dir));
        //tree.setToggleClickCount(2);
        // Add a listener
        popup = new PopUpMenu(frame);
        tree.addTreeSelectionListener(new TreeSelectionListener() {
            @Override
            public void valueChanged(TreeSelectionEvent e) {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.getPath().getParentPath().getLastPathComponent();
                frame.setTitle(node + " - UMHDL");
                project = node + "";
                System.out.println("You selected " + node);
            }
        });

        ImageIcon folder_icon = new ImageIcon("src"+File.separator+"resources"+File.separator+"folder_1_14x14.png");
        ImageIcon file_icon = new ImageIcon("src"+File.separator+"resources"+File.separator+"fileicon_15x15.gif");
        DefaultTreeCellRenderer renderer = new DefaultTreeCellRenderer();
            renderer.setOpenIcon(folder_icon);
            renderer.setClosedIcon(folder_icon);
            renderer.setLeafIcon(file_icon);
        tree.setCellRenderer(renderer);
        tree.setRootVisible(false);
        frame.panel_arbol = new JScrollPane(tree);
        
        frame.add(frame.panel_arbol, BorderLayout.WEST);
        tree.addMouseListener(ml);
    }
    MouseListener ml = new MouseAdapter() {
        @Override
        public void mousePressed(MouseEvent e) {

            selRow = tree.getRowForLocation(e.getX(), e.getY());
            selPath = tree.getPathForLocation(e.getX(), e.getY());
            if (selRow != -1) {
                if (e.getClickCount() == 1) {
                    if (selPath.getPathCount() == 2) {
                        //System.out.println(selPath + " ---- " + selPath.getParentPath() + selPath.getLastPathComponent());
                        frame.setTitle(selPath.getLastPathComponent() + " - UMHDL");
                        frame.ws.setWorkspaceWorkingPath(frame.ws.getWorkspaceRootPath(), selPath.getLastPathComponent() + File.separator);
                        frame.log.setText("");
                    } else if (selPath.getPathCount() == 3) {
                        //System.out.println("Project: " + project);
                        frame.ws.setWorkspaceWorkingPath(frame.ws.getWorkspaceRootPath(), selPath.getParentPath().getLastPathComponent() + File.separator);
                        //System.out.println(selPath.getParentPath().getLastPathComponent());
                        frame.log.setText("");
                        //System.out.println("Filename: " + selPath.getLastPathComponent());
                        filename = "" + selPath.getLastPathComponent();
                        if (SwingUtilities.isRightMouseButton(e)) {
                            tree.setSelectionRow(selRow);
                            popup.show(e.getComponent(), e.getX(), e.getY());
                        }
                    } else if (selPath.getPathCount() > 3) {
                        frame.log.setText("\n - Warning! You are not at the root of the project!");
                    }

                } else if (e.getClickCount() == 2) {
                    openFile();
                }
            }
        }
    };

    public void openFile() {
        //System.out.println("Project: " + project);
        frame.ws.setWorkspaceWorkingPath(frame.ws.getWorkspaceRootPath(), project + File.separator);
        //System.out.println(selPath + " ---- " + selPath.getLastPathComponent());
        frame.text.setName(frame.getExtension(selPath.getLastPathComponent().toString()));
        try {
            Load_File load_file = new Load_File(frame.ws.getWorkspaceWorkingPath() + File.separator + selPath.getLastPathComponent(), frame);
        } catch (IOException ex) {
            Logger.getLogger(FileTree.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Add nodes from under directory into curTop. Highly recursive.
     */
    final DefaultMutableTreeNode addNodes(DefaultMutableTreeNode curTop, File dir) {
        String curPath = dir.getPath();
        DefaultMutableTreeNode curDir = new DefaultMutableTreeNode(dir.getName());
        if (curTop != null) { // should only be null at root
            curTop.add(curDir);
        }
        Vector ol = new Vector();
        String[] tmp = dir.list();
        for (int i = 0; i < tmp.length; i++) {
            ol.addElement(tmp[i]);
        }
        Collections.sort(ol, String.CASE_INSENSITIVE_ORDER);
        File file;
        Vector files = new Vector();
        // Make two passes, one for Dirs and one for Files. This is #1.
        for (int i = 0; i < ol.size(); i++) {
            String thisObject = (String) ol.elementAt(i);
            String newPath;
            if (curPath.equals(".")) {
                newPath = thisObject;
            } else {
                newPath = curPath + File.separator + thisObject;
            }
            if ((file = new File(newPath)).isDirectory()) {
                addNodes(curDir, file);
            } else {
                files.addElement(thisObject);
            }
        }
        // Pass two: for files.
        for (int fnum = 0; fnum < files.size(); fnum++) {
            //Filter by extension
            if (frame.getExtension((String) files.elementAt(fnum)).equals("vhd") || frame.getExtension((String) files.get(fnum)).equals("v")) {
                curDir.add(new DefaultMutableTreeNode(files.elementAt(fnum)));
            }
        }
        return curDir;
    }

    public void deleteFile(String filename) {
        File file = new File(frame.ws.getWorkspacePath() + frame.ws.getProjectFolder() + filename);
        if (file.delete()) {
            //Delete node JTree
            MutableTreeNode node = (MutableTreeNode) selPath.getLastPathComponent();
            MutableTreeNode parent = (MutableTreeNode) node.getParent();
            int index = parent.getIndex(node);
            parent.remove(node);
            DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
            model.nodesWereRemoved(parent, new int[]{index}, null);
            for (int i = 0; i < frame.tabbedPane.getTabCount(); i++) {
                if (frame.tabbedPane.getTitleAt(i).equals(filename)) {
                    frame.tabbedPane.remove(i);
                    frame.vector_textArea.remove(i);
                    //System.out.println("ROOT: "+frame.getWorkspaceRootPath());
                    //System.out.println("PROYECTO: "+frame.getFileNameWithout_TB(frame.getFileName(frame.tabbedPane.getTitleAt(frame.tabbedPane.getSelectedIndex()))));
                    frame.ws.setWorkspaceWorkingPath(frame.ws.getWorkspaceRootPath(), frame.ws.getFileNameWithout_TB(frame.ws.getFileName(frame.tabbedPane.getTitleAt(frame.tabbedPane.getSelectedIndex()))) + File.separator);
                }
            }
        }
    }
}