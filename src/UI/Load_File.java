package UI;

/**
 *
 * @author mario
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import org.gjt.sp.jedit.Mode;
import org.gjt.sp.jedit.syntax.ModeProvider;
import org.gjt.sp.jedit.textarea.TextArea;

public class Load_File extends JFrame {

    JTabbedPane tabbedPane;

    Load_File(String directory, MiFrame frame) throws IOException {

        this.tabbedPane = frame.tabbedPane;
        File archivo = new File(directory);
        FileReader fileR = new FileReader(archivo);

        BufferedReader lecturaTeclado = new BufferedReader(fileR);
        // Lectura del fichero
        String line, content = "";

        while ((line = lecturaTeclado.readLine()) != null) {
            content += line + "\n";
        }

        boolean existe = false;
        if (frame.tabbedPane.getTabCount() == 1 && frame.tabbedPane.getTitleAt(tabbedPane.getSelectedIndex()).equals("blank")) {
            TextArea text = (TextArea) frame.vector_textArea.get(tabbedPane.getSelectedIndex());
            if (frame.text.getText().equals("")) {
                frame.vector_textArea.remove(tabbedPane.getSelectedIndex());
                frame.tabbedPane.remove(tabbedPane.getSelectedIndex());
            }
        } else {
            for (int i = 0; i < tabbedPane.getTabCount(); i++) {
                if (tabbedPane.getTitleAt(i).equals(archivo.getName())) {
                    tabbedPane.setSelectedIndex(i);
                    existe = true;
                    break;
                }
            }
        }

        if (!existe) {
            String tipoArchivo = "vhdl";
            TextArea text = MiFrame.createTextArea();
            //frame.text=text;
            if (frame.text.getName().equals(".vhd")) {
                tipoArchivo = "vhdl";
            } else if (frame.text.getName().equals(".v")) {
                tipoArchivo = "verilog";
            }
            System.out.println(tipoArchivo);
            frame.text = text;
            frame.mode_text = new Mode("vhdl.xml");
            frame.mode_text.setProperty("file", "src"+File.separator+"modes"+File.separator+"" + tipoArchivo + ".xml");
            ModeProvider.instance.addMode(frame.mode_text);
            frame.text.getBuffer().setMode(frame.mode_text);
            frame.text.setName(text.extension);
            frame.text.content = content;
            frame.text.setText(text.content);
            frame.vector_textArea.add(frame.text);
            System.out.println("Tamaño: " + frame.vector_textArea.size());
            frame.scrollPane = new JScrollPane();
            frame.scrollPane.setViewportView(text);
            frame.addCloseTab(archivo.getName(), frame.scrollPane);
            frame.getContentPane().add(tabbedPane);
            frame.tabbedPane.setSelectedIndex(/*tabbedPane.getSelectedIndex()+ index_offset*/tabbedPane.getTabCount() - 1);
        }
        if (null != fileR) {
            fileR.close();
        }
    }
}