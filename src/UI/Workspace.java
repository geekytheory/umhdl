/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import javax.swing.WindowConstants;

/**
 *
 * @author mario
 */
public class Workspace {
    
    private static String CONFIG_FILE = "configuration.properties";
    private static String WORKSPACE_PATH = "UMHDL";  // "Code Editor Workspace"
    private static String PROJECT_FOLDER = "";
    
    private Properties prop;
    
    public String workspace_path;
    public String project_folder;
    
    // Constructor
    public Workspace () {
        // Creamos objetos
        prop = new Properties();
        // Valores por defecto
        workspace_path = System.getProperty("user.home") + File.separator + WORKSPACE_PATH + File.separator;
        //project_folder = PROJECT_FOLDER;
    }
    
    // Leemos las propiedades desde el archivo CONFIG_FILE
    public boolean load () {
        FileInputStream fis;
        
        try {
            // Intenemos abrir el archivo de configuracion
            fis = new FileInputStream(CONFIG_FILE);
            //set the properties value
            prop.load(fis);
            workspace_path = prop.getProperty("workspace_path");
            project_folder = prop.getProperty("project_folder");
            fis.close();
            if (!workspace_path.endsWith(File.separator)) {
                workspace_path += File.separator;
            }
            return (true);
    	} catch (IOException e1) {
            // Fichero no existe
            return (false);
        }
    }
    
    // Grabamos las propiedades al archivo de configuracion
    public boolean save () {
        FileOutputStream fos;
        
        // Actualizamos las propiedades
        prop.setProperty("workspace_path", workspace_path);
        prop.setProperty("project_folder", project_folder);
        //prop.setProperty("firstRun", "1");
        // Escribimos el fichero
        try {
            //save properties to project root folder
            fos = new FileOutputStream(CONFIG_FILE);
            prop.store(fos, null);
            fos.close();
            return (true);
        } catch (IOException e2) {
            //System.out.println("ERROR");
            return (false);
        }
    }
    
    // Establece la ruta del workspace
    public void setWorkspacePath(String workspace_path) {
        this.workspace_path = workspace_path.trim();
        if (!this.workspace_path.isEmpty() &&
            !this.workspace_path.endsWith(File.separator)) {
            this.workspace_path += File.separator;
        }
        // save ();
    }
    
    // Devuelve la ruta del workspace
    public String getWorkspacePath() {
        return (workspace_path);
    }
    
    // Establece el directorio del proyecto
    /*public void setProjectFolder(String project_folder) {
        this.project_folder = project_folder.trim();
        if (!this.project_folder.isEmpty() &&
            !this.project_folder.endsWith(File.separator)) {
            this.project_folder += File.separator;
        }
        // save ();
    }*/
    public void setWorkspaceWorkingPath(String workspace, String folder) {
        Properties prop = new Properties();

        try {
            //set the properties value
            prop.load(new FileInputStream("configuration.properties"));
            prop = new Properties();
            //prop.setProperty("firstRun", "1");
            prop.setProperty("workspace_path", workspace);
            prop.setProperty("project_folder", folder);
            prop.store(new FileOutputStream("configuration.properties"), null);
            //save properties to project root folder

        } catch (IOException ex) {
            System.out.println("ERROR");
        }
    }
    
    // Devuelve el directorio del proyecto
    public String getProjectFolder() {
        return (project_folder);
    }
    
    // Devuelve el nombre del proyecto (quitando el último caracter /)
    /*public String getActiveProject() {
        if (project_folder.isEmpty()) {
            return ("");
        } else if (project_folder.endsWith(File.separator)) {
            return (project_folder.substring(0, project_folder.length()-1));
        } else {
            return (project_folder);
        }
    }*/
    
    public String getActiveProject() {
        Properties prop = new Properties();
        String project_returned="";
        try {
            //set the properties value
            prop.load(new FileInputStream("configuration.properties"));
            String project = prop.getProperty("project_folder");
            project_returned = project.substring(0, project.length() - 1);
        } catch (Exception ex) {
            System.out.println("ERROR");  
        }
        return project_returned; 
    }
    
    public String getFileName(String fileName) {
        String filename = "";
        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            filename = fileName.substring(0, i);
        }
        return filename;
    }
    
    public String getFileNameWithout_TB(String fileName){
        String filename = fileName;
        int i = fileName.lastIndexOf("_tb");
        if (i > 0) {
            filename = fileName.substring(0, i);
        }
        return filename;
    }
    
    public String getWorkspaceRootPath() { //Devuelve el directorio en el que está trabajando
        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream("configuration.properties"));
            String path1 = prop.getProperty("workspace_path");
            return path1;
        } catch (IOException ex) {
            System.out.println("ERROR");
        }
        return null;
    }
    
    // Devuelve el directorio completo en el que está trabajando
    public String getWorkspaceWorkingPath() {
        String working_path = getWorkspaceRootPath() + getActiveProject();
        return (working_path);
    }
    
    
        
    // Pedimos directorio para el workspace
    public String askWorkspacePath (java.awt.Frame frame) {
        WorkspacePathDialog dialog;
        
        frame.setVisible(false);
        dialog = new WorkspacePathDialog(frame, true);
        dialog.setVisible(true);  // Invoca a setWorkspacePath al aceptar
        return (workspace_path);
    }
}


/**
 *
 * @author mario
 */
class WorkspacePathDialog extends javax.swing.JDialog {

    private static String TITLE = "Set workspace path";
    
    /**
     * Creates new form WorkspacePathDialog
     */
    MiFrame frame;
    public WorkspacePathDialog(java.awt.Frame parent, boolean modal) {
        super(parent, TITLE, modal);
        initComponents();
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        this.frame=(MiFrame) parent;
        if(!frame.ws.getWorkspaceWorkingPath().equals("")){
            jTextField2.setText(frame.ws.getWorkspaceWorkingPath());
            jButton3.setEnabled(true);
        } else {
            jTextField2.setText(System.getProperty("user.home")+File.separator+"UMHDL");
            jButton3.setEnabled(false);
        }
        setResizable(false);
        setLocation(400, 50);
    }

   
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField2 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTextField2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField2ActionPerformed(evt);
            }
        });

        jLabel5.setText("Directory:");

        jButton2.setText("ACCEPT");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("CANCEL");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 357, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton2)
                        .addGap(18, 18, 18)
                        .addComponent(jButton3)))
                .addContainerGap(21, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addGap(18, 18, 18)
                .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(jButton3))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField2ActionPerformed
        
    }//GEN-LAST:event_jTextField2ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        File workspace_path=new File(jTextField2.getText());
        workspace_path.mkdir();
        frame.setVisible(true);
        this.dispose();
        frame.ws.setWorkspacePath(jTextField2.getText());  // pablo
        Properties prop;
    	try {
    		//set the properties value    		
                    prop = new Properties();
                    prop.setProperty("workspace_path", jTextField2.getText()+isLastCharSlash(jTextField2.getText()));
                    prop.store(new FileOutputStream("configuration.properties"), null);
    		//Save properties to project root folder
    	} catch (IOException ex) {
            System.out.println("ERROR");
        }
            frame.refreshTree(frame);          
        }//GEN-LAST:event_jButton2ActionPerformed
    
    
    public String isLastCharSlash(String path){ //Puede que el usuario escriba "/" o no.
        if(path.charAt(path.length()-1)==File.separatorChar){
            return "";
        }
        return File.separator;
    }

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_jButton3ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(WorkspacePathDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(WorkspacePathDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(WorkspacePathDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(WorkspacePathDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                WorkspacePathDialog dialog = new WorkspacePathDialog(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JTextField jTextField2;
    // End of variables declaration//GEN-END:variables
}