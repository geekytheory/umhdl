package UI;

import static UI.MiFrame.new_in_mode;
import static UI.MiFrame.save_as;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

/**
 *
 * @author mario
 */
public class CreateImageIcons {
    public CreateImageIcons(MiFrame frame) {
        ImageIcon new_file_icon = new ImageIcon("src"+File.separator+"resources"+File.separator+"document_new_small.png");
            MiFrame.new_file = new JMenuItem("New", new_file_icon);
            new_in_mode = new JMenu("New in mode");
            MiFrame.new_in_mode.setIcon(new_file_icon);
        ImageIcon new_file_icon_TB = new ImageIcon("src"+File.separator+"resources"+File.separator+"new_file_25x25.png");
            frame.new_file_TB.setIcon(new_file_icon_TB);
        ImageIcon verilog_icon = new ImageIcon("src"+File.separator+"resources"+File.separator+"verilog.gif");
            MiFrame.Mode_Verilog = new JMenuItem("Verilog", verilog_icon);
        ImageIcon vhd_icon = new ImageIcon("src"+File.separator+"resources"+File.separator+"vhd.gif");
            MiFrame.Mode_VHDL = new JMenuItem("VHDL", vhd_icon);
        ImageIcon openfile_icon = new ImageIcon("src"+File.separator+"resources"+File.separator+"openfile_16x16.png");
            MiFrame.open = new JMenuItem("Open", openfile_icon);
        ImageIcon openfile_icon_TB = new ImageIcon("src"+File.separator+"resources"+File.separator+"openfile_25x25.png");
            frame.openfile_TB.setIcon(openfile_icon_TB);
        ImageIcon savefile_icon = new ImageIcon("src"+File.separator+"resources"+File.separator+"savefile_16x16.png");
            MiFrame.save = new JMenuItem("Save", savefile_icon);
            save_as = new JMenuItem("Save as", savefile_icon);
        ImageIcon savefile_icon_TB = new ImageIcon("src"+File.separator+"resources"+File.separator+"savefile_25x25.png");
            frame.save_TB.setIcon(savefile_icon_TB);        
        ImageIcon simulate_icon = new ImageIcon("src"+File.separator+"resources"+File.separator+"simulate_16x16.png");
            MiFrame.simulate = new JMenuItem("Simulate", simulate_icon);
        ImageIcon simulate_icon_TB = new ImageIcon("src"+File.separator+"resources"+File.separator+"simulate_25x25.png");
            frame.simulate_TB.setIcon(simulate_icon_TB);
        ImageIcon exit_icon = new ImageIcon("src"+File.separator+"resources"+File.separator+"exit_16x16.png");
            MiFrame.exit = new JMenuItem("Exit", exit_icon);
        ImageIcon pdf_icon = new ImageIcon("src"+File.separator+"resources"+File.separator+"pdf_16x16.png");
            MiFrame.export_pdf = new JMenuItem("Export to PDF", pdf_icon);
        ImageIcon pdf_icon_TB = new ImageIcon("src"+File.separator+"resources"+File.separator+"pdf_25x25.png");
            frame.export_pdf_TB.setIcon(pdf_icon_TB);
        ImageIcon redo_icon_TB = new ImageIcon("src"+File.separator+"resources"+File.separator+"redo_25x25.png");
        ImageIcon undo_icon_TB = new ImageIcon("src"+File.separator+"resources"+File.separator+"undo_25x25.png");
        ImageIcon compile_icon_TB = new ImageIcon("src"+File.separator+"resources"+File.separator+"compile_25x25.png");
            frame.compile_TB.setIcon(compile_icon_TB);
        ImageIcon gtkwave_icon_TB = new ImageIcon("src"+File.separator+"resources"+File.separator+"gtkwave_25x25.png");
            frame.gtkwave_TB.setIcon(gtkwave_icon_TB);
        ImageIcon newProject_icon_TB = new ImageIcon("src"+File.separator+"resources"+File.separator+"newproject_25x25.png");
            frame.newProject_TB.setIcon(newProject_icon_TB);
        ImageIcon newProject_icon = new ImageIcon("src"+File.separator+"resources"+File.separator+"newproject_16x16.png");
            MiFrame.new_project=new JMenuItem("New project", newProject_icon);
    }
}
