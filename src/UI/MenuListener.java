/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import com.itextpdf.text.DocumentException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.gjt.sp.jedit.textarea.TextArea;

/**
 *
 * @author mario
 */
public class MenuListener implements ActionListener{
    MiFrame frame;
    FileTree tree;
    MenuListener(MiFrame frame){
        this.frame=frame;
    }
    public void actionPerformed(ActionEvent e) {
        tree= frame.listado_directorios;
        if(e.getSource()==tree.popup.open_PU){
            tree.openFile();
        } else if(e.getSource()==frame.change_workspace){
            WorkspacePathDialog dialog=new WorkspacePathDialog(frame, true);
            dialog.show();
        } else if (e.getSource()==tree.popup.compile_PU) {
            System.out.println("Compile "+tree.project+" - "+tree.filename);
            frame.checkSyntax(tree.project, tree.filename);
        } else if (e.getSource()==tree.popup.delete_PU) {
            tree.deleteFile(tree.filename);
        } else if (e.getSource() == frame.new_file || e.getSource() == frame.new_file_TB) {
            JDialog_New_File nuevo = new JDialog_New_File(frame, true);
        } else if (e.getSource() == frame.Mode_VHDL) {
            System.out.println("MODE VHDL");
        } else if (e.getSource() == frame.Mode_Verilog) {
            System.out.println("MODE VERILOG");
        } else if (e.getSource() == MiFrame.open || e.getSource() == frame.openfile_TB ) {
            try {
                try {
                    Directorios directorios = new Directorios("Load", frame, null);
                } catch (DocumentException ex) {
                    Logger.getLogger(MiFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (IOException ex) {
                Logger.getLogger(MiFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (e.getSource() == frame.save_as) {
            int indice;
            indice = frame.tabbedPane.getSelectedIndex();
            TextArea texto = (TextArea) frame.vector_textArea.get(indice);
            this.frame.text = texto;
            this.frame.text.content = this.frame.text.getText();
            System.out.println("Contenido: " + frame.text.content);
            try {
                try {
                    Directorios directorios = new Directorios("Save", frame, "Save");
                } catch (DocumentException ex) {
                    Logger.getLogger(MiFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (IOException ex) {
                Logger.getLogger(MiFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (e.getSource() == frame.save || e.getSource() == frame.save_TB) {
            int indice;
            indice = frame.tabbedPane.getSelectedIndex();
            TextArea texto;
            if (frame.tabbedPane.getTitleAt(indice).equals("blank")) {
                texto = (TextArea) frame.vector_textArea.get(frame.tabbedPane.getSelectedIndex());
            } else {
                texto = (TextArea) frame.vector_textArea.get(frame.tabbedPane.getSelectedIndex());
            }
            this.frame.text = texto;
            this.frame.text.content = this.frame.text.getText();
            String filename = frame.tabbedPane.getTitleAt(indice);
            String extension = null;
            int i = filename.lastIndexOf('.');
            if (i > 0) {
                extension = filename.substring(i + 1);
            }
            System.out.println(extension);
            frame.text.setName(extension);
            if (frame.text.getName() != null) {
                try {
                    try {
                        Save_File save_file = new Save_File(frame.ws.getWorkspacePath() +frame.ws.getFileNameWithout_TB(frame.ws.getFileName(frame.tabbedPane.getTitleAt(frame.tabbedPane.getSelectedIndex())))+File.separator+ frame.tabbedPane.getTitleAt(frame.tabbedPane.getSelectedIndex()), frame, "Save", false);
                    } catch (DocumentException ex) {
                        Logger.getLogger(MiFrame.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } catch (IOException ex) {
                    Logger.getLogger(MiFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                try {
                    try {
                        Directorios directorios = new Directorios("Save", frame, "Save");
                    } catch (DocumentException ex) {
                        Logger.getLogger(MiFrame.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } catch (IOException ex) {
                    Logger.getLogger(MiFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else if (e.getSource() == frame.export_pdf || e.getSource() == frame.export_pdf_TB) {
            try {
                try {
                    Directorios directorios = new Directorios("Save_PDF", frame, "Save");
                } catch (DocumentException ex) {
                    Logger.getLogger(MiFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (IOException ex) {
                Logger.getLogger(MiFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (e.getSource() == frame.exit) {
            for (int i = 0; i < frame.tabbedPane.getTabCount(); i++) {
                TextArea texto = (TextArea) frame.vector_textArea.get(i);
                this.frame.text = texto;
                if (!texto.getText().equals(texto.content)) {
                    System.out.println("Archivo " + texto.name + texto.extension + " modificado");
                }
            }
        } else if (e.getSource() == frame.simulate || e.getSource() == frame.simulate_TB) {
            String project = frame.ws.getActiveProject();
            File archivo = new File(frame.ws.getWorkspaceWorkingPath() + project + ".vhd");
            String extension, filename;
            if (archivo.exists()) {
                extension = ".vhd";
            } else {
                extension = ".v";
            }
            filename = project + extension;
            try {
                frame.s = null;
                frame.log_info = "";
                frame.log_info += "\n  " + frame.getDate() + " - Building Project: " + project + "\n\n";
                frame.log_info += "\t~$ ghdl -a " + filename;
                frame.Simulate("ghdl -a " + filename, true);
                frame.log_info += "\t~$ ghdl -e " + frame.ws.getFileName(filename);
                frame.Simulate("ghdl -e " + frame.ws.getFileName(filename), true);
                frame.log_info += "\t~$ ghdl -r " + frame.ws.getFileName(filename);
                frame.Simulate("ghdl -r " + frame.ws.getFileName(filename), true);
                if (frame.s == null) {
                    frame.log.append("\n" + frame.buildOK());
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(MiFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (e.getSource() == frame.compile_TB) {
            frame.checkSyntax(frame.ws.getFileName(frame.tabbedPane.getTitleAt(frame.tabbedPane.getSelectedIndex())), frame.tabbedPane.getTitleAt(frame.tabbedPane.getSelectedIndex()));
        } else if (e.getSource() == frame.gtkwave_TB) {
            String project = frame.ws.getActiveProject();
            File archivo = new File(frame.ws.getWorkspaceWorkingPath() + project + ".vhd");
            String extension, filename;
            if (archivo.exists()) {
                extension = ".vhd";
            } else {
                extension = ".v";
            }
            filename = project + extension;
            try {
                frame.s = null;
                frame.log_info = "";
                frame.log_info += "\n  " + frame.getDate() + " - Building Project: " + project + "\n\n";
                frame.log_info += "\t~$ ghdl -a " + filename;
                frame.Simulate("ghdl -a " + filename, true);

                frame.log_info += "\t~$ ghdl -a " + frame.ws.getFileName(filename) + "_tb.vhd";
                frame.Simulate("ghdl -a " + frame.ws.getFileName(filename) + "_tb.vhd", true);

                frame.log_info += "\t~$ ghdl -e " + frame.ws.getFileName(filename);
                frame.Simulate("ghdl -e " + frame.ws.getFileName(filename), true);

                frame.log_info += "\t~$ ghdl -e " + frame.ws.getFileName(filename) + "_tb";
                frame.Simulate("ghdl -e " + frame.ws.getFileName(filename) + "_tb", true);

                frame.log_info += "\t~$ ghdl -r " + frame.ws.getFileName(filename) + "_tb";
                frame.Simulate("ghdl -r " + frame.ws.getFileName(filename) + "_tb", false);

                frame.log_info += "\t~$ ghdl -r " + frame.ws.getFileName(filename) + "_tb --vcd=" + frame.ws.getFileName(filename) + ".vcd";
                frame.Simulate("ghdl -r " + frame.ws.getFileName(filename) + "_tb --vcd=" + frame.ws.getFileName(filename) + ".vcd", false);

                frame.log_info += "\t~$ gtkwave " + frame.ws.getFileName(filename) + ".vcd";
                frame.Simulate("gtkwave " + frame.ws.getFileName(filename) + ".vcd", false);
                if (frame.s == null) {
                    frame.log.append("\n" + frame.buildOK());
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(MiFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (e.getSource() == frame.newProject_TB || e.getSource() == frame.new_project) {
            System.out.println("Nuevo proyecto");
            NewProjectDialog dialog = new NewProjectDialog(frame, false);
        } else {
            System.out.println("NO RECONOCIDO");
        }
    }
}
