package UI;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.WindowConstants;
import org.gjt.sp.jedit.IPropertyManager;
import org.gjt.sp.jedit.Mode;
import org.gjt.sp.jedit.syntax.ModeProvider;
import org.gjt.sp.jedit.textarea.StandaloneTextArea;
import org.gjt.sp.jedit.textarea.TextArea;
import org.gjt.sp.util.IOUtilities;
import org.gjt.sp.util.Log;

import com.itextpdf.text.DocumentException;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author mario
 */
public final class MiFrame extends JFrame {

    static JMenuBar barra;
    static JMenu file, new_in_mode, options;
    static JMenuItem change_workspace, new_project, new_file, Mode_VHDL, Mode_Verilog, open, save, save_as, export_pdf, exit, simulate, compile;
    FileTree listado_directorios;
    JTabbedPane tabbedPane;
    JScrollPane scrollPane;
    ArrayList<TextArea> vector_textArea;
    TextArea text;
    Mode mode_text;
    JToolBar toolbar;
    JTextArea log;
    JScrollPane scrollpane;
    String log_info = "";
    JButton new_file_TB, save_TB, export_pdf_TB, openfile_TB, simulate_TB, compile_TB, gtkwave_TB, newProject_TB;
    //CONSTRUCTOR
    JScrollPane panel_arbol;
    MenuListener listener;
    
    Workspace ws;
    File workspaceFiles;
    public MiFrame() throws DocumentException {
        super();
        WindowUtilities.setNativeLookAndFeel();
        toolbar = toolbar();
        CreateImageIcons iconos = new CreateImageIcons(this);
        add(toolbar, BorderLayout.NORTH);
        setJMenuBar(crearMenu());
        text = createTextArea();
        vector_textArea = new ArrayList();
        setLocation(350, 50);
        mode_text = new Mode("vhdl.xml");
        mode_text.setProperty("file", "src"+File.separator+"modes"+File.separator+"vhdl.xml");
        ModeProvider.instance.addMode(mode_text);
        text.getBuffer().setMode(mode_text);
        text.content = text.getText();
        vector_textArea.add(text);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pack();
        setTitle("UMHDL");
        ImageIcon imgFrame = new ImageIcon("src"+File.separator+"resources"+File.separator+"txt_file.png");
        setIconImage(imgFrame.getImage());
        tabbedPane = new JTabbedPane();
        scrollPane = new JScrollPane();
        scrollPane.setViewportView(text);
        //tabbedPane.addTab("blank", scrollPane);
        getContentPane().add(tabbedPane, BorderLayout.CENTER);
        setSize(800, 650);
        
        ws = new Workspace();  // pablo
        // Intentamos leer el archivo de configuracion
        /*if (!ws.load()) {
            // Si no existe (primera ejecucion) nos pide ruta workspace y graba fichero
            ws.askWorkspacePath(this);
            ws.save();
        }  */
        File workpath=new File(ws.getWorkspaceRootPath());
        if(!workpath.exists()) {
            WorkspacePathDialog dialog=new WorkspacePathDialog(this, true);
            dialog.show();
        }
        
        //ConfigIDE config = new ConfigIDE(this);       
        
        log = new JTextArea("", 8, 20);
        scrollpane = new JScrollPane(log);
        add(scrollpane, BorderLayout.SOUTH);
        
        workspaceFiles = new File(ws.getWorkspacePath());   // pablo
        listado_directorios = new FileTree(workspaceFiles, this);
        
        tabbedPane.addChangeListener(new ChangeListener() {
        public void stateChanged(ChangeEvent e) {
            System.out.println("Tab: " + tabbedPane.getSelectedIndex());
            if(tabbedPane.getSelectedIndex()!=-1){ //Evitar error al iniciarse el programa
                // pablo
                ws.setWorkspaceWorkingPath(ws.getWorkspaceRootPath(), ws.getFileNameWithout_TB(ws.getFileName(tabbedPane.getTitleAt(tabbedPane.getSelectedIndex())))+File.separator);
                setTitle("UMHDL :: " + ws.getActiveProject());
            }
        }
    });
        addCloseTab("blank", scrollPane);
        show();
    }
    
    public void refreshTree(MiFrame frame){
        frame.remove(frame.panel_arbol);
            //frame.panel_arbol.remove(frame.listado_directorios.tree);
            frame.listado_directorios = new FileTree(new File(frame.ws.getWorkspaceRootPath()), frame);
            frame.show();
    }

    public void addCloseTab(String title, final JScrollPane Scrollpane){
        System.out.println("TABS - "+tabbedPane.getTabCount());
        tabbedPane.addTab(title, Scrollpane);
        FlowLayout f=new FlowLayout(FlowLayout.CENTER, 5, 0);
        JPanel pnlTab=new JPanel(f);
        pnlTab.setOpaque(false);
        JLabel lblTitle=new JLabel(title);
        //Diferenciar entre VHDL y Verilog
        lblTitle.setIcon(new ImageIcon("src"+File.separator+"resources"+File.separator+"vhd.gif"));
        JButton btnClose=new JButton();
        btnClose.setOpaque(false);
        btnClose.setIcon(new ImageIcon("src"+File.separator+"resources"+File.separator+"closetab_11x11.png"));
        btnClose.setBorder(null);
        btnClose.setFocusable(false);
        pnlTab.add(lblTitle);
        pnlTab.add(btnClose);
        pnlTab.setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));
        System.out.println("TABS - "+tabbedPane.getTabCount());
            if(tabbedPane.getTabCount()>0){
                tabbedPane.setTabComponentAt(tabbedPane.getTabCount()-1, pnlTab);
            }
        
        ActionListener listener = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
            tabbedPane.remove(Scrollpane);
            vector_textArea.remove(tabbedPane.getSelectedIndex()); 
      }
    };
    btnClose.addActionListener(listener);
    }
    
    private JToolBar toolbar() {
        toolbar = new JToolBar();
        new_file_TB = new JButton();
        newProject_TB = new JButton();
        save_TB = new JButton();
        openfile_TB = new JButton();
        simulate_TB = new JButton();
        export_pdf_TB = new JButton();
        compile_TB = new JButton();
        gtkwave_TB = new JButton();
        new_file_TB.setToolTipText("New file");
        newProject_TB.setToolTipText("New project");
        save_TB.setToolTipText("Save");
        openfile_TB.setToolTipText("Open file");
        simulate_TB.setToolTipText("Simulate");
        export_pdf_TB.setToolTipText("Export to PDF");
        compile_TB.setToolTipText("Build Project");
        gtkwave_TB.setToolTipText("Show waveform");
        
        listener=new MenuListener(this);
        
        new_file_TB.addActionListener(listener);
        newProject_TB.addActionListener(listener);
        save_TB.addActionListener(listener);
        openfile_TB.addActionListener(listener);
        simulate_TB.addActionListener((ActionListener) listener);
        export_pdf_TB.addActionListener((ActionListener) listener);
        compile_TB.addActionListener((ActionListener) listener);
        gtkwave_TB.addActionListener((ActionListener) listener);
        toolbar.add(new_file_TB);
        toolbar.add(newProject_TB);
        toolbar.add(save_TB);
        toolbar.add(openfile_TB);
        toolbar.add(export_pdf_TB);
        toolbar.add(compile_TB);
        toolbar.add(simulate_TB);
        toolbar.add(gtkwave_TB);
        return toolbar;
    }

    protected JPanel createInnerPanel(String text) {
        JPanel jplPanel = new JPanel();
        JLabel jlbDisplay = new JLabel(text);
        jlbDisplay.setHorizontalAlignment(JLabel.CENTER);
        jplPanel.setLayout(new GridLayout(1, 1));
        jplPanel.add(jlbDisplay);
        return jplPanel;
    }

    private static Properties loadProperties(String fileName) {
        Properties props = new Properties();
        File file;
        if (fileName.charAt(0) == '/') {
            file = new File(fileName.substring(1));
        } else {
            file = new File(fileName);
        }

        InputStream in = null;
        try {
            if (file.isFile()) {
                in = new FileInputStream(file);
            } else {
                in = TextArea.class.getResourceAsStream(fileName);
            }
            props.load(in);
        } catch (IOException e) {
            Log.log(Log.ERROR, TextArea.class, e);
        } finally {
            IOUtilities.closeQuietly(in);
        }
        return props;
    }

    public static StandaloneTextArea createTextArea() {
        final Properties props = new Properties();
        props.putAll(loadProperties("/keymaps/jEdit_keys.props"));
        props.putAll(loadProperties("/org/gjt/sp/jedit/jedit.props"));
        StandaloneTextArea textArea = new StandaloneTextArea(new IPropertyManager() {
            public String getProperty(String name) {
                return props.getProperty(name);
            }
        });
        textArea.getBuffer().setProperty("folding", "explicit");
        return textArea;
    } // }}}

    /**
     *
     * @return
     */
    private JMenuBar crearMenu() {
        barra = new JMenuBar();
        file = new JMenu("File");
            new_project.addActionListener((ActionListener) listener);
            new_file.addActionListener((ActionListener) listener);
            new_in_mode.addActionListener((ActionListener) listener);
                Mode_VHDL.addActionListener((ActionListener) listener);
                Mode_Verilog.addActionListener((ActionListener) listener);
            open.addActionListener((ActionListener) listener);
            open.setAccelerator(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.Event.CTRL_MASK));
            save.addActionListener((ActionListener) listener);
            save.setAccelerator(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.Event.CTRL_MASK));
            save_as.addActionListener((ActionListener) listener);
            save_as.setAccelerator(KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.Event.CTRL_MASK | java.awt.Event.SHIFT_MASK));
            simulate.addActionListener((ActionListener) listener);
            export_pdf.addActionListener((ActionListener) listener);
            exit.addActionListener((ActionListener) listener);
            
        options = new JMenu("Options");
            change_workspace=new JMenuItem("Change workspace");
            change_workspace.addActionListener(listener);
            options.add(change_workspace);
        file.add(new_project);
        file.add(new_file);
        new_in_mode.add(Mode_VHDL);
        new_in_mode.add(Mode_Verilog);
        file.add(new_in_mode);
        file.add(save);
        file.add(save_as);
        file.add(open);
        file.add(simulate);
        file.add(export_pdf);
        file.addSeparator();
        file.add(exit);
        barra.add(file);
        barra.add(options);
        return barra;
    }

    
    ////////
    Process p;
    String s;
    ////////

    public String buildOK() {
        String build_OK = "BUILD SUCCESSFUL";
        JLabel label = new JLabel(build_OK);
        label.setFont(new Font("Helvetica", Font.BOLD, 10));
        return label.getText();
    }

    public void Simulate(String command, boolean gtkwave) throws InterruptedException {
        try {
            if (gtkwave) {
                p = Runtime.getRuntime().exec(command, null, new File(ws.getWorkspaceWorkingPath()));
                p.waitFor();
                // Get input streams
                BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
                BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

                // Read command standard output

                //System.out.println("Standard output: ");
                //String log_info = null;
                while ((s = stdInput.readLine()) != null) {
                    log_info = log_info + "\n - " + s;
                    System.out.println(log_info);
                }

                while ((s = stdError.readLine()) != null) {
                    log_info = log_info + "\n - " + s;
                    System.out.println(log_info);
                }
                System.out.println(log_info);
                log.setText(log_info);
                log_info += "\n";
                stdError.close();
                p.destroy();
            } else {
                MyRunner myRunner = new MyRunner(ws.getWorkspaceWorkingPath(), command);
                Thread myThread = new Thread(myRunner);
                myThread.start();
            }
            
        } catch (IOException e1) {
        }

        
        System.out.println("Done");
    }

    public String getDate() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss ");
        Date date = new Date();
        return dateFormat.format(date);
    }
    
    /*
    public String getActiveProject() {
        Properties prop = new Properties();

        try {
            //set the properties value
            prop.load(new FileInputStream("configuration.properties"));
            String project = prop.getProperty("project_folder");
            return project.substring(0, project.length() - 1);
        } catch (IOException ex) {
            System.out.println("ERROR");
        }
        return null;
    }
    */

    /*
    public final String getWorkspaceWorkingPath() { //Devuelve el directorio en el que está trabajando
        Properties prop = new Properties();

        try {
            //set the properties value
            prop.load(new FileInputStream("configuration.properties"));
            String path1 = prop.getProperty("workspace_path");
            String path2 = prop.getProperty("project_folder");
            return path1 + path2;
        } catch (IOException ex) {
            System.out.println("ERROR");
        }
        return null;
    }

    public void setWorkspaceWorkingPath(String workspace, String folder) {
        Properties prop = new Properties();

        try {
            //set the properties value
            prop.load(new FileInputStream("configuration.properties"));
            prop = new Properties();
            prop.setProperty("firstRun", "1");
            prop.setProperty("workspace_path", workspace);
            prop.setProperty("project_folder", folder);
            prop.store(new FileOutputStream("configuration.properties"), null);
            //save properties to project root folder

        } catch (IOException ex) {
            System.out.println("ERROR");
        }
    }

    public String getWorkspaceRootPath() { //Devuelve el directorio en el que está trabajando
        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream("configuration.properties"));
            String path1 = prop.getProperty("workspace_path");
            return path1;
        } catch (IOException ex) {
            System.out.println("ERROR");
        }
        return null;
    }
    */

    public String getExtension(String fileName) {
        String extension = "";
        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            extension = fileName.substring(i + 1);
        }
        return extension;
    }

    /*public String getFileName(String fileName) {
        String filename = "";
        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            filename = fileName.substring(0, i);
        }
        return filename;
    }*/
    
    /*public String getFileNameWithout_TB(String fileName){
        String filename = fileName;
        int i = fileName.lastIndexOf('_');
        if (i > 0) {
            filename = fileName.substring(0, i);
        }
        return filename;
    }*/
    
    public void checkSyntax(String projectname, String filename){
    try {
                s = null;
                log_info = "";
                //String project = getFileName(tabbedPane.getTitleAt(tabbedPane.getSelectedIndex()));
                //String file = tabbedPane.getTitleAt(tabbedPane.getSelectedIndex());
                
                String project=projectname;
                String file=filename;
                log_info += "\n  " + getDate() + " - Building Project: " + project + "\n\n";
                log_info += "\t~$ ghdl -a " + filename;
                Simulate("ghdl -a " + filename, true);
                log_info += "\t~$ ghdl -e " + ws.getFileName(filename);
                Simulate("ghdl -e " + ws.getFileName(filename), true);
                if (s == null) {
                    log.append("\n" + buildOK());
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(MiFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
}

class MyRunner implements Runnable {
    Process p;
    String PATH, command;

    MyRunner(String PATH, String command) {
        this.PATH = PATH;
        this.command = command;
    }

    public void run() {
        try {
            p = Runtime.getRuntime().exec(command, null, new File(PATH));
        } catch (IOException ex) {
            Logger.getLogger(MyRunner.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            p.waitFor();
        } catch (InterruptedException ex) {
            Logger.getLogger(MyRunner.class.getName()).log(Level.SEVERE, null, ex);
        }
        p.destroy();
    }
}